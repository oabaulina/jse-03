# Информация о проекте

jse-03

## Стек технологий

java/Intellij IDEA/Git

## Требования к SOFTWARE

- JDK 1.8

## Команда для запуска проекта

```bash
java -jar ./jse-03.jar
```

## Информация о разработчике

**ФИО**: Баулина Ольга Александровна

**E-MAIL**: golovolomkacom@gmail.com